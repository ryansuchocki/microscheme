;; Hello World
;; Ryan Suchocki
;; microscheme.org

(include "libraries/lcd.ms")
(lcd-init)

(print "Hello World!")
